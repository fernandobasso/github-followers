# Github Followers

- [Github Followers](#github-followers)
  - [Introduction](#introduction)
  - [Project setup](#project-setup)
    - [Start local server for development](#start-local-server-for-development)
    - [Run unit tests](#run-unit-tests)
    - [Build for production](#build-for-production)
    - [Lints and fixes files](#lints-and-fixes-files)
  - [Versions](#versions)
  - [Project overview](#project-overview)
  - [Technical considerations](#technical-considerations)
    - [Overall architecture](#overall-architecture)
    - [Component responsibility](#component-responsibility)
    - [Code style](#code-style)
    - [Unit tests](#unit-tests)
  - [Future improvements and ideas](#future-improvements-and-ideas)

## Introduction

An application to search for and display a Github user and their followers. Hosted on a Heroku free dyno.

https://ghfolw.herokuapp.com/



## Project setup

We use NVM to manage Node versions. Enter the project root directory and type:

```
$ nvm install
```

And then:

```
$ npm install
```

**NOTE**: Make sure you run `nvm use` (inside the project root directory) before your run any of the following commands.

### Start local server for development

```
$ npm run serve
```

### Run unit tests
```
$ npm run test:unit
$ npm run test:unit -- --watch --coverage
```

### Build for production
```
$ npm run build
```

### Lints and fixes files
```
$ npm run lint
```

## Versions

We use Node 12.16.1, which is the latest LTS version as of this writing (checked with `nvm ls-remot --lts | tail -n 5)`. There is a `.nvmrc` file at the root directory of the project. 

**NOTE**: Make sure you run `nvm use` (inside the project root directory) before your run any of the above commands.

For the version of Vue.js itself and other packages, check `package.json` file itself.

## Project overview

Built using Vue.js from the Vue CLI. It uses SCSS for styling, and Jest and Vue Test Utils for unit tests.

![Project Preview 1](./docs/imgs/preview1.png)



Vue CLI generated this boilerplate:

```text
~/Projects/github-followers [(ef0b7de...) ✔]

$ tree -C -I 'node_modules|coverage'
.
├── babel.config.js
├── package.json
├── package-lock.json
├── public
│   ├── favicon.ico
│   └── index.html
├── README.md
└── src
    ├── App.vue
    ├── assets
    │   └── logo.png
    ├── components
    │   └── HelloWorld.vue
    └── main.js
```

Then I added `.editorconfig`, `.nvmrc`, changed `min.js`, `App.vue`, removed `HelloWorld.vue` and some other stuff, and proceeded from there to the initial implementation.

## Technical considerations

What follows is an overview of the technical decisions taken. I am not a Vue.js specialist, and I last worked with Vue.js about 1.5 years ago.

### Overall architecture

For this application, since it does not (so far, at least) require routing and navigating to other pages, there was no strong reason to have a separate views (pages) to include the other components that compose and make up an entire page. Therefore,  the main App component is responsible for fetching data and providing the children components with what they need to be rendered on the viewing medium.

So far, because it is a relatively simple and small, there has not been compelling justification to handle the state of the application in a Vuex store.

That being said, we could perhaps have used Vuex nonetheless, as well as create a Followers page and make use of Vue Router from the beginning, although it could be considered a case of useless over engineering.

### Component responsibility

One thing that could be further improved is the way data is fetched. I'm somewhat used to make container and presentational components in React, and in this project, since it had been 1.5 years since I last worked with Vue.js, I did not know very well how to do it, and decided on a simpler approach.

### Code style

I tend to prefer a more functional programming approach, and using things like `this.foo.bar.tux` is not a style of programming that I find appealing. Most stuff inside the components could avoid that sort chaining by using destructuring assignment, like:

```
const { foo: { bar: { tux } } } = this;
```

The inherent and necessary use of `this` in Vue component makes it harder to attempt a more functional style. The [Composition API](https://github.com/vuejs/rfcs/blob/function-apis/active-rfcs/0000-function-api.md) (which is currently being discussed) is likely to provide a better chance to avoid this long chaining style or long and deeply-nested destructuring assignment situation.

Rantings aside, the project uses Airbnb style for linting and I tend not to like to rely on automatic semicolon insertion too much, and generally prefer code with semicolons. JavaScript is not Python :laughing:.

PS: I have no problem working with any code style whatsoever. I just have my own preferences when I have a say on this mater and I am creating a project myself.

### Unit tests

The majority of the tests focus on the public API of the components. We test component input (props, events and user interactions), and component output (rendered DOM, emitted events, and external function calls), rather than focusing on the implementation details internal to the components.

This way, components that just change the internal implementation but not the behavior (for example, to improve readability) do not cause tests to fail, and it makes it easier to adapt tests to future requirement changes to the components. Besides that, focusing on the components contract while unit testing makes tests more similar how real users interact with the components, causing the tests to more closely reflect the real world.

![Unit Tests Preview](./docs/imgs/unit-tests1.png)

PS: I do not think that 100% test coverage is always a win. It takes time to design, implement and maintain **correct** tests. There are trade-offs to be made, and 100% coverage does not guarantee bug-free production applications. Yet, the more coverage, the more the changes are of finding and easily destroying bugs before they reach the users.

![Alien Queen Bug](./docs/imgs/alien-queen1.png)



## Future improvements and ideas

* Improve error handling.

* Add loading animations and/or transitions while requests are in flight for a more pleasant look and feel.
* Add e2e tests.
* Improve the theme, and perhaps add a theme switcher.











