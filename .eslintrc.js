
/**
 * Checks whether the current env is one of the given envs.
 *
 * @param {string} currEnv
 * @param  {...string} envsAllowed
 */
function isAllowedEnv(currEnv, ...envsAllowed) {
  return envsAllowed.includes(currEnv);
}

/**
 * @var {string}
 */
const ENV = process.env.NODE_ENV;

module.exports = {
  root: true,

  env: {
    node: true,
  },

  extends: [
    'plugin:vue/essential',
    '@vue/airbnb',
  ],

  parserOptions: {
    parser: 'babel-eslint',
  },

  rules: {
    'no-console': isAllowedEnv(ENV, 'development') ? 'off' : 'error',
    'no-debugger': isAllowedEnv(ENV, 'development') ? 'off' : 'error',
    'no-unused-vars': isAllowedEnv(ENV, 'development') ? 'off' : 'error',

    // Some situations in Vue.js REQUIRE that we DO NOT use arrow functions
    // in order to retain the proper value of ‘this’.
    'object-shorthand': 'off',
    'arrow-body-style': 'off',
    camelcase: 'off',
  },

  overrides: [
    {
      files: [
        '**/__tests__/*.{j,t}s?(x)',
        '**/tests/unit/**/*.spec.{j,t}s?(x)',
      ],
      env: {
        jest: true,
      },
    },
  ],
};
