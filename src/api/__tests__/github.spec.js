import axios from 'axios';

import { fetchUser, fetchFollowers } from '@/api/github';

jest.mock('axios');

const yoda = { login: 'yoda', avatar_url: 'yoda.jpg', followers: 3 };
const luke = { login: 'luke', avatar_url: 'luke.png', followers: 2 };
const leia = { login: 'leia', avatar_url: 'leia.jpg', followers: 0 };
const obiwan = { login: 'obi-wan', avatar_url: 'obi-wan.png', followers: 1 };

describe('github-api.js', () => {
  describe('fetchUser()', () => {
    it('should not blow-up with non-existing users', () => {
      axios.get.mockImplementationOnce(() => Promise.resolve({ message: 'Not Found' }));
      return fetchUser('non-existing').then(({ message }) => {
        expect(message).toMatch(/not found/i);
      });
    });

    it('should fetch an user', () => {
      axios.get.mockImplementationOnce(() => Promise.resolve(yoda));
      return expect(fetchUser('yoda')).resolves.toEqual(yoda);
    });

    it('should handle rejections', () => {
      axios.get.mockImplementationOnce(() => Promise.reject(new Error('Network Error')));
      return expect(fetchUser('rejected')).rejects.toEqual(new Error('Network Error'));
    });
  });

  describe('fetchFollowers()', () => {
    it('should not blow-up with non-existing user', () => {
      axios.get.mockImplementationOnce(
        () => Promise.resolve({ data: { message: 'Not found' } }),
      );
      return expect(
        fetchFollowers({ login: 'non-existing' }),
      ).resolves.toEqual({ data: { message: 'Not found' } });
    });

    it('should fetch an user followers', async (done) => {
      axios.get.mockImplementationOnce(() => [luke, leia]);
      const requestPayload = { login: 'yoda', pageNum: 1, perPage: 2 };
      const followers = await fetchFollowers(requestPayload);
      expect(followers).toHaveLength(2);
      expect(followers).toEqual([luke, leia]);
      done();
    });

    it('should fetch a different number of followers per page', async (done) => {
      axios.get.mockImplementationOnce(() => [luke, leia, obiwan]);
      const requestPayload = { login: 'yoda', pageNum: 1, perPage: 3 };
      const followers = await fetchFollowers(requestPayload);
      expect(followers).toHaveLength(3);
      done();
    });
  });
});
