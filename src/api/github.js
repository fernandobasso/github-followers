import axios from 'axios';

const githubApi = axios.create({
  baseURL: 'https://api.github.com',
  headers: {
    Accept: 'application/vnd.github.v3+json',
    Authorization: 'token 6dddc4b36d90dfabe025766bfec7c666c826824d',
  },
});

const fetchUser = (login) => {
  return githubApi.get(`/users/${login}`);
};

const fetchFollowers = ({ login, pageNum, perPage }) => {
  const url = `/users/${login}/followers?page=${pageNum}&per_page=${perPage}`;
  return githubApi.get(url);
};

export {
  fetchUser,
  fetchFollowers,
};
