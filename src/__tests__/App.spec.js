import axios from 'axios';
import { shallowMount, mount } from '@vue/test-utils';

import App from '@/App.vue';
import Search from '@/components/Search.vue';
import UserNotFound from '@/components/UserNotFound.vue';
import User from '@/components/User.vue';
import Followers from '@/components/Followers.vue';

jest.mock('axios');

const yoda = { login: 'yoda', avatar_url: 'yoda.jpg', followers: 3 };
const luke = { login: 'luke', avatar_url: 'luke.png', followers: 2 };
const leia = { login: 'leia', avatar_url: 'leia.jpg', followers: 0 };
const obiwan = { login: 'obi-wan', avatar_url: 'obi-wan.png', followers: 1 };

describe('App.vue', () => {
  it('should render', () => {
    const wrp = shallowMount(App);
    expect(wrp.is(App)).toBe(true);
    expect(wrp.find('h1').text()).toEqual('Github Followers');
  });

  it('should display the search form component', () => {
    const wrp = shallowMount(App);
    expect(wrp.contains(Search)).toBe(true);
  });

  it('should display the followers component', () => {
    const wrp = shallowMount(App);
    expect(wrp.contains(Followers)).toBe(true);
  });

  it('should have correct text for load more button', async (done) => {
    const wrp = shallowMount(App);

    expect(wrp.vm.btnMoreText).toEqual('No user searched');

    wrp.setData({
      user: yoda,
      followers: [obiwan],
    });

    wrp.vm.$nextTick();
    expect(wrp.vm.btnMoreText).toEqual('Load more');

    wrp.setData({
      user: yoda,
      followers: [obiwan, luke, leia],
    });

    await wrp.vm.$nextTick();

    expect(wrp.vm.btnMoreText).toEqual('No more followers to fetch');

    done();
  });

  it('should handle btn load more clicks', async (done) => {
    axios.get.mockImplementationOnce(() => ({ data: [leia] }));
    const wrp = mount(App);
    wrp.setData({
      user: yoda,
      followers: [luke],
    });

    await wrp.vm.$nextTick();

    wrp.find('.btn-load-more').trigger('click');

    await wrp.vm.$nextTick();

    expect(wrp.vm.followers.length).toEqual(2);

    done();
  });

  it('should reset search data when another user is searched', async (done) => {
    axios.get.mockImplementationOnce(() => ({ status: 200, data: yoda }));
    axios.get.mockImplementationOnce(() => ({ status: 200, data: luke }));

    const wrp = mount(App, {
      attachToDocument: true,
    });

    wrp.find('.search-input').setValue('yoda');
    await wrp.vm.$nextTick();

    wrp.find('.search-submit').trigger('click');
    await wrp.vm.$nextTick();
    await wrp.vm.$nextTick();

    expect(wrp.find(User).text()).toContain('yoda');

    wrp.find('.search-input').setValue('luke');
    wrp.find('.search-submit').trigger('click');

    expect(wrp.find(Search).emitted('submitted-search').length).toEqual(1);

    done();
    wrp.destroy();
  });

  it('should handle non-existing users', async (done) => {
    axios.get.mockImplementationOnce(() => {
      return Promise.resolve({ status: 404, data: { message: 'Not found' } });
    });

    const wrp = mount(App, {
      attachToDocument: true,
    });

    wrp.find('.search-input').setValue('darth-vader');
    await wrp.vm.$nextTick();

    wrp.find('.search-submit').trigger('click');
    await wrp.vm.$nextTick();
    await wrp.vm.$nextTick();

    expect(wrp.find(User).exists()).toBe(false);
    expect(wrp.find(UserNotFound).exists()).toBe(true);
    expect(wrp.find(UserNotFound).props('login')).toEqual('darth-vader');

    done();
    wrp.destroy();
  });
});
