import { shallowMount } from '@vue/test-utils';
import Followers from '@/components/Followers.vue';
import Follower from '@/components/Follower.vue';

const yoda = { login: 'yoda', avatar_url: 'yoda.jpg' };
const luke = { login: 'luke', avatar_url: 'luke.png' };

describe('Followers.vue', () => {
  it('should render without crashing', () => {
    const wrp = shallowMount(Followers);
    expect(wrp.is(Followers)).toBe(true);
  });

  it('should render the title', () => {
    const wrp = shallowMount(Followers);
    expect(wrp.find('h2').text()).toEqual('Followers');
  });

  it('should render followers', () => {
    const wrp = shallowMount(Followers, {
      propsData: {
        followers: [yoda, luke],
      },
    });

    expect(wrp.findAll(Follower)).toHaveLength(2);
  });
});
