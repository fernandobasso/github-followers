import { mount } from '@vue/test-utils';
import Follower from '@/components/Follower.vue';

describe('Follower.vue', () => {
  it('should render correctly', () => {
    const wrp = mount(Follower, {
      propsData: {
        follower: {
          login: 'ahsoka-tano',
          avatar_url: 'ahsokatano.png',
          html_url: 'https://github.com/ahsokatano',
        },
      },
    });
    expect(wrp).toMatchSnapshot();
  });
});
