import { shallowMount } from '@vue/test-utils';
import User from '@/components/User.vue';

describe('User.vue', () => {
  const user = {
    login: 'master-yoda',
    avatar_url: 'https://avatars3.githubusercontent.com/u/6560620?v=4',
  };

  it('should render without crashing', () => {
    expect(shallowMount(User).isEmpty()).toBe(true);
    expect(shallowMount(User, { propsData: { } }).isEmpty()).toBe(true);
  });

  it('should render the user login', () => {
    const wrp = shallowMount(User, {
      propsData: { user },
    });
    expect(wrp.text()).toMatch('master-yoda');
  });

  it('should render the user avatar', () => {
    const wrp = shallowMount(User, {
      propsData: { user },
    });

    const img = wrp.find('img');

    expect(img.attributes('src')).toEqual(user.avatar_url);
    expect(img.attributes('alt')).toEqual(user.login);
  });
});
