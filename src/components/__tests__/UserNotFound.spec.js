import { shallowMount } from '@vue/test-utils';
import UserNotFound from '@/components/UserNotFound.vue';

describe('UserNotFound.vue', () => {
  it('should display the not found message with the login', () => {
    const wrp = shallowMount(UserNotFound, {
      propsData: { login: 'aayla-secura' },
    });

    expect(wrp.find('p').text()).toEqual('User ‘aayla-secura’ not found.');
  });

  it('should render with the correct structure', () => {
    const wrp = shallowMount(UserNotFound, {
      propsData: { login: 'ahsoka-tano' },
    });

    expect(wrp).toMatchSnapshot();
  });
});
