import { shallowMount } from '@vue/test-utils';
import Search from '@/components/Search.vue';

describe('Search.vue', () => {
  it('should render without crashing', () => {
    const wrp = shallowMount(Search);
    expect(wrp.is(Search));
  });

  it('should display the search form input field', () => {
    const wrp = shallowMount(Search);
    const form = wrp.find('form');
    const input = form.find('input');
    const submit = form.find('button');

    expect(input.attributes('placeholder')).toEqual('Type github login');
    expect(submit.text()).toEqual('Search');
  });

  it('should not emit event when login is empty', async (done) => {
    const wrp = shallowMount(Search, {
      attachToDocument: true,
    });

    const submit = wrp.find('button');
    const input = wrp.find('input');
    input.setValue(' ');
    submit.trigger('click');
    await wrp.vm.$nextTick();

    expect(wrp.emitted('submitted-search')).toBeFalsy();

    done();

    wrp.destroy();
  });

  it('should emit event when login is filled-in', async (done) => {
    const wrp = shallowMount(Search, {
      attachToDocument: true,
    });

    const input = wrp.find('.search-input');
    const submit = wrp.find('.search-submit');

    input.setValue('yoda');
    await wrp.vm.$nextTick();

    submit.trigger('click');

    expect(wrp.emitted('submitted-search').length).toEqual(1);

    done();
    wrp.destroy();
  });

  it('should make disabled true when login is empty', () => {
    const wrp = shallowMount(Search);

    const input = wrp.find('.search-input');

    expect(wrp.vm.disabled).toEqual(true);

    input.setValue('sdras');
    expect(wrp.vm.disabled).toEqual(false);

    input.setValue('');
    expect(wrp.vm.disabled).toEqual(true);
  });

  it('should disable submit button when loading requests', async (done) => {
    const wrp = shallowMount(Search, {
      propsData: {
        loading: true,
      },
    });

    wrp.setData({ login: 'torvalds' });

    const submit = wrp.find('.search-submit');
    expect(submit.attributes('disabled')).toEqual('disabled');

    wrp.setProps({ loading: false });
    await wrp.vm.$nextTick();

    expect(submit.attributes('disabled')).toBeFalsy();

    done();
  });

  it('should disable submit button while input value remains the same', async (done) => {
    const wrp = shallowMount(Search, {
      attachToDocument: true,
      propsData: {
        loading: false,
      },
    });

    const input = wrp.find('.search-input');
    const submit = wrp.find('.search-submit');

    input.setValue('ahsoka-tano');
    await wrp.vm.$nextTick();

    // When input is filled but not submitted, should not be disabled.
    expect(submit.attributes('disabled')).toBeFalsy();

    submit.trigger('click');
    await wrp.vm.$nextTick();

    // After a submit, it is disabled.
    expect(submit.attributes('disabled')).toEqual('disabled');

    // And after changing to something else without submitting, it
    // should not be disabled.
    input.setValue('aayla-secura');
    await wrp.vm.$nextTick();
    expect(wrp.vm.$data.login).not.toEqual(wrp.vm.$data.prevLogin);
    expect(submit.attributes('disabled')).toBeFalsy();

    // We do not click submit. That is on purpse because we only update
    // ‘prevLogin’ when the user actually submits the form.
    input.setValue('ahsoka-tano');
    await wrp.vm.$nextTick();
    expect(submit.attributes('disabled')).toEqual('disabled');

    done();
    wrp.destroy();
  });
});
