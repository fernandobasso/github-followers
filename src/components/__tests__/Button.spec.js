import { shallowMount } from '@vue/test-utils';

import Button from '@/components/Button.vue';

describe('Button.vue', () => {
  it('should render without crashing', () => {
    const wrp = shallowMount(Button, {
      propsData: { evtName: 'clicked' },
    });
    expect(wrp.is(Button));
  });

  it('should display text from the prop', () => {
    const wrp = shallowMount(Button, {
      propsData: {
        text: 'A Button',
        evtName: 'clicked',
      },
    });
    expect(wrp.text()).toEqual('A Button');
  });

  it('should contain default button type', () => {
    const wrp = shallowMount(Button, {
      propsData: { evtName: 'clicked' },
    });
    expect(wrp.attributes('type')).toEqual('button');
  });

  it('should have the provided css class', () => {
    const wrp = shallowMount(Button, {
      propsData: {
        evtName: 'clicked',
        className: 'the-ultimate-button',
      },
    });

    expect(wrp.attributes('class')).toContain('the-ultimate-button');
  });

  it('should not be disabled by default', () => {
    const wrp = shallowMount(Button, {
      propsData: { evtName: 'clicked' },
    });
    expect(wrp.attributes('disabled')).toBeFalsy();
  });

  it('should be in disabled state according to prop', async (done) => {
    const wrp = shallowMount(Button, {
      propsData: {
        disabled: true,
        evtName: 'clicked',
      },
    });
    expect(wrp.attributes('disabled')).toEqual('disabled');

    wrp.setProps({ disabled: false });
    await wrp.vm.$nextTick();
    expect(wrp.attributes('disabled')).toBeFalsy();

    done();
  });

  it('emit submitted event when clicked', async (done) => {
    const wrp = shallowMount(Button, {
      propsData: { evtName: 'my-btn-clicked' },
    });

    wrp.trigger('click');
    wrp.vm.$nextTick();

    expect(wrp.emitted('my-btn-clicked')).toBeTruthy();

    done();
  });
});
