const yoda = {
  login: 'yoda',
  name: 'Master Yoda',
  avatar_url: 'yoda.jpg',
  followers: 3,
};

const luke = {
  login: 'luke',
  name: 'Luke Skywalker',
  avatar_url: 'luke.png',
  followers: 2,
};
const leia = {
  login: 'leia',
  name: 'Leia Organa',
  avatar_url: 'leia.jpg',
  followers: 0,
};

const obiwan = {
  login: 'obi-wan',
  name: 'Obi-wan Kenobi',
  avatar_url: 'obi-wan.png',
  followers: 1,
};
