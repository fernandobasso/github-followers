/* eslint-disable no-undef */
const mockedAxios = jest.genMockFromModule('axios');

//
// Fixes ‘axios.create()’ undefined error.
//
mockedAxios.create = jest.fn(() => mockedAxios);

export default mockedAxios;
